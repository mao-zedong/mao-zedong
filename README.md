### Mao Zedong [Tse-tung], 毛泽东 ###

-----


- [mao-zedong.net](http://mao-zedong.net)


- [Windows-qt](https://mega.nz/#!ctB0XZaY!yORVYVzA7SPfSbGEvd7cpz79WWqVwDWQxfUvwYC9LUA)


- [Blockexplorer](http://explorer.mao-zedong.net:3001)





-----

### Road map ###

1, yobit.net via addcoin


2, World domination

-----

![bitcoin.png](https://bitbucket.org/repo/yp89q4d/images/2306709604-bitcoin.png)




### "The Chairman." ###

----

![splash.png](https://bitbucket.org/repo/yp89q4d/images/2181426147-splash.png)


-----



### Mao Zedong ###

-----

**Specs**


Algo: x11, Hybrid (14,000 blocks) / 

POS (operating from block 1)

-----

Coin: Mao Zedong

Ticker: MAO

-----

**Block Parameters**


Block Spacing: 480 seconds

Difficulty re-targeting: Each block

-----

**Ports**


rpcport=9669

port=9670

-----

**Rewards**


**POS**

Annual interest rate: 5% 

Staking minimum age: 12 hours

Staking maximum age: 12 days

-----

**POW** 

From 1 -14,000 height

888 coins each block

14,000 x 888

= 12,432,000

*Adjustment for PoS mined blocks*

minus 25% (prediction)

= - 3,108,000

-----

**Prediction of total coins in 12 weeks (full PoS):**

= 12,432,000 - 3,108,000

= 9,324,000

*+ 1,000,000 (premine)*

**= 10,324,000**



-----

![splash_testnet.png](https://bitbucket.org/repo/yp89q4d/images/4176752740-splash_testnet.png)

-----

[![N|Solid](http://www.kabulmagazine.com/wp-content/uploads/2016/12/mao-poster-revolution1-600x414.jpg)](https://nodesource.com/products/nsolid)

-----

![bkg.png](https://bitbucket.org/repo/yp89q4d/images/1798069237-bkg.png)


-----

> Tags: Mao Zedong, Mao Tse-tung, Chairman, Communist Party of China, 

> Great Leap Forward [Future], Great Proletarian Cultural Revolution

> 毛泽东, 大跃进, 无产阶级文化大革命

-----


[Website](http://mao-zedong.net)